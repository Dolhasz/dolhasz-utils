import dolhasz as ad
import numpy as np
import datetime
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt


def test_imread():
    print('Testing loading of images')
    img = ad.image.read('./test_img.jpg', dtype='uint')
    assert img.dtype == np.uint8
    assert np.max(img) <= 255

    print('Testing conversion to float on load')
    img = ad.image.read('./test_img.jpg', dtype='float')
    assert img.dtype == np.float
    assert np.max(img) <= 1.0

    print('Testing reshaping on input')
    img = ad.image.read('./test_img.jpg', dtype='float', resize=(50, 100))
    assert img.dtype == np.float
    assert img.shape == (100, 50, 3)


def test_imwrite():
    print('Testing saving of images')
    image = ad.image.read('./test_img.jpg')
    ad.image.write(image, './test_output.png')
    new_image = ad.image.read('./test_output.png')
    assert np.array_equal(image, new_image)

    print('Testing implicit float to uint conversion on save')
    image = ad.image.read('./test_img.jpg', dtype='float')
    ad.image.write(image, './test_output.png')
    new_image = ad.image.read('./test_output.png', dtype='float')
    assert np.array_equal(image, new_image)
    

def test_squeeze_stretch():
    print('Testing expand_dims switch')
    image = ad.image.read('./test_img.jpg')
    stretched = ad.image.stretch(image)
    squeezed = ad.image.squeeze(stretched)
    assert (1, *image.shape) == stretched.shape
    assert image.shape == squeezed.shape


def test_colour_transfer():
    source = ad.image.read('./test_img.jpg')
    target = ad.image.read('./test_img.jpg')
    target[:,:,2] = target[:,:,1]
    target[:,:,1] = source[:,:,2]
    result, source, target = ad.image.color_transfer(source, target)
    assert result.shape == source.shape == target.shape


def test_coco_generator():
    gen = ad.data.COCOGenerator('F:/_________COCO_________', n_samples=None, batch_size=8, image_shape=(224,224,3))
    start = datetime.datetime.now()
    X, Ys = gen[0]
    print(datetime.datetime.now() - start)


def test_harmonization_generator():

    image_shape = (224, 224, 3)
    test_shape = (1, *image_shape)

    for dataset in ad.data.HARMONIZATION_DATASETS:
        gen = ad.data.HarmonizationGenerator('H:/image_harmonization/', dataset=dataset, batch_size=1, image_shape=image_shape)
        for batch in gen:
            X, Y = batch
            image, mask = Y
            assert X.shape == test_shape
            assert image.shape == test_shape
            assert mask.shape == (*test_shape[:-1], 1)
            f, ax = plt.subplots(1,3)
            ax[0].imshow(X.reshape(image_shape))
            ax[1].imshow(image.reshape(image_shape))
            ax[2].imshow(mask.reshape(image_shape[:2]))
            plt.show()
            break

        
if __name__ == '__main__':
    test_coco_generator()

