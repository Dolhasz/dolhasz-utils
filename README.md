# 1. Installation

- Clone repository <BR>


- Create conda environment
```
conda env create -f ./environment.yml -n your_env_name
```
- Install cython manually (as it is a dick and doesn't want to install as part of pip or conda)
```
pip install cython
```
- Install the remaining requirements:
```
pip install -r ./requirements.txt
```


# 2. Usage

Run unit tests:
```
pytest ./test_lib.py
```

To import the library run:
```
import dolhasz as ad
```