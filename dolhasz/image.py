from warnings import warn
import cv2
import numpy as np 


def read(path: str, dtype: str = 'uint', resize: tuple = None, gray=False):
    '''Loads RGB image from path'''
    img = cv2.imread(path)
    if gray:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    else:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    if 'float' in dtype:
        # warn('image.imwrite: Converting image from uint to float')
        img = img / 255.0
    if resize is not None:
        img = cv2.resize(img, resize)
    return img 


def write(image: np.array, path: str, resize: tuple = None):
    '''Saves RGB image to path (png)'''
    # if np.sum(image) == 0.0:
    #     warn('image.imwrite: Image is empty!')
    if image.dtype != np.uint8:
        # warn('image.imwrite: Converting image from float to uint')
        if np.max(image) <= 1.0:
            image = (image * 255)
        image = image.astype(np.uint8)
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    cv2.imwrite(path, image)


def squeeze(image: np.array):
    '''Wrapper for np.squeeze'''
    return np.squeeze(image)


def stretch(image: np.array):
    '''Wrapper for expand dims on axis 0'''
    return np.expand_dims(image, axis=0)


def get_mean_and_std(x, mask=None):
    x_mean, x_std = cv2.meanStdDev(x, mask=mask)
    x_mean = np.hstack(np.around(x_mean,2))
    x_std = np.hstack(np.around(x_std,2))
    return x_mean, x_std


def color_transfer(source, target, source_mask=None, target_mask=None):
    '''
    Apply Reinhard et al. colour transfer to entire image or masked regions.
    '''
    original = source.copy()
    source = cv2.cvtColor(source, cv2.COLOR_RGB2LAB)
    target = cv2.cvtColor(target, cv2.COLOR_RGB2LAB)
    s_mean, s_std = get_mean_and_std(source, source_mask)
    t_mean, t_std = get_mean_and_std(target, target_mask)

    height, width, channel = source.shape
    for i in range(0,height):
        for j in range(0,width):
            for k in range(0,channel):
                if source_mask is not None and target_mask is not None:
                    if source_mask[i,j] == 0:
                        continue
                    else:
                        x = source[i,j,k]
                        x = ((x-s_mean[k])*(t_std[k]/s_std[k]+0.0000001))+t_mean[k]
                        x = round(x)
                        x = 0 if x<0 else x
                        x = 255 if x>255 else x
                        if np.isnan(x):
                            x = 0.0
                        source[i,j,k] = x

    result = cv2.cvtColor(source, cv2.COLOR_LAB2RGB)
    target = cv2.cvtColor(target, cv2.COLOR_LAB2RGB)
    return result, original, target


def normalize(img, min=0, max=1.0):
    return (img - np.min(img)) / (np.max(img) - np.min(img))


def preprocess_input(x):
    if isinstance(x, list):
        return [img * 2 - 1.0 for img in x]
    else:
        return x * 2.0 - 1.0