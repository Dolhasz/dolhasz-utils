from comet_ml import Experiment
import datetime
import os
import datetime
import logging
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt


class NeuralNetworkConfig:
    '''
    Config class for NeuralNetworkExperiment objects
    '''
    def __init__(self):
        self.EXPERIMENT_CLASS = 'VGG16AE'
        self.EPOCHS = 100
        self.BATCH_SIZE = 8
        self.LEARNING_RATE = 0.0001

        self.IMAGE_SHAPE = (224,224,3)

        self.MULTIPROCESSING = True
        self.MP_WORKERS = 4

        self.COMET_API_KEY = None
        self.COMET_PROJECT = None


class Logger:
    '''
    Basic logger for tensorflow. 
    Spawn a new instance for additional scalars
    '''
    def __init__(self, logdir=None, train=True):
        assert logdir is not None, 'You must specify log dir'
        subfolder = 'training' if train else 'validation'
        self.logdir = os.path.join(
            logdir, 
            datetime.datetime.now().strftime("%Y%m%d-%H%M%S"), 
            subfolder
        )
        print(f'Logging to {self.logdir}')
        os.makedirs(self.logdir)
        self.file_writer = tf.summary.create_file_writer(self.logdir)

    def log_value(self, name, value, step):
        with self.file_writer.as_default():
            tf.summary.scalar(name, value, step)
            self.file_writer.flush()

    def log_image(self, name, image, step):
        with self.file_writer.as_default():
            tf.summary.image(name, image, step, max_outputs=8)
            self.file_writer.flush()

    def log_hist(self, gradients, step):
        with self.file_writer.as_default():
            [tf.summary.histogram("%s-grad" % idx, g[0], step) for idx, g in enumerate(gradients)]
            self.file_writer.flush()


def create_train_batch():
    @tf.function
    def train_batch(model, optimizer, loss_fn, x_train, y_train):
        # Forward pass
        with tf.GradientTape() as tape:
            logits = model(x_train, training=True)
            loss_value = loss_fn(y_train, logits)

        # Backprop
        grads = tape.gradient(loss_value, model.trainable_weights)
        optimizer.apply_gradients(zip(grads, model.trainable_weights))
        del tape
        return logits, tf.reduce_mean(loss_value), grads
    return train_batch

@tf.function
def train_batch_mask_sup(model, optimizer, loss_fn, x_train, y_train):
    assert len(loss_fn) == 2 == len(y_train)

    image, mask = y_train
    rec_loss, mask_loss = loss_fn

    # Forward pass
    with tf.GradientTape() as tape:
        rec_logits, mask_sigmoid = model(x_train, training=True)
        rec_loss = rec_loss(image, rec_logits)
        mask_loss = mask_loss(mask, mask_sigmoid)

    total_loss = (rec_loss + mask_loss) / 2 
    # Backprop
    grads_all = tape.gradient(total_loss, model.trainable_weights, unconnected_gradients=tf.UnconnectedGradients.ZERO)
    # grads_ptc = tape.gradient(mask_loss, )
    optimizer.apply_gradients(zip(grads_all, model.trainable_weights))
    # optimizer.apply_gradients(zip(grads_ptc, model.get_layer('sigmoid_ptc').trainable_weights))
    del tape
    return [rec_logits, mask_sigmoid], tf.reduce_mean(total_loss), grads_all

def create_val_batch():
    @tf.function
    def validate_batch(model, loss_fn, x_val, y_val):
        val_logits = model(x_val, training=False)
        val_loss = tf.reduce_mean(loss_fn(y_val, val_logits))
        return val_logits, val_loss
    return validate_batch

class TrainingLoop:
    def __init__(self, model, loss_fn, optimizer, 
    train_gen, val_gen, config, logdir=None, 
    savedir=None, update=1, metrics=None):
        # Model and config
        self.model = model
        self.config = config
        self.n_train_steps = len(train_gen)
        self.n_val_steps = len(val_gen)

        # Training Multithreading 
        self.train_q = tf.keras.utils.OrderedEnqueuer(train_gen)
        self.train_q.start(
            workers=self.config.MP_WORKERS, 
            max_queue_size=self.config.MP_MAX_QUEUE_SIZE
            )
        self.train_gen = self.train_q.get()
        # Validation Multithreading 
        self.val_q = tf.keras.utils.OrderedEnqueuer(val_gen)
        self.val_q.start(
            workers=self.config.MP_WORKERS, 
            max_queue_size=self.config.MP_MAX_QUEUE_SIZE
            )
        self.val_gen = self.val_q.get()

        # Optimizer
        self.loss_fn = loss_fn
        self.optimizer = optimizer
        self.metrics = metrics

        self.train_batch = create_train_batch()
        self.validate_batch = create_val_batch()

        # Dirs
        if savedir is not None:
            self.save_dir = savedir
        if logdir is not None:
            self.train_logger = Logger(logdir=logdir, train=True)
            self.val_logger = Logger(logdir=logdir, train=False)
    
    def train_epoch(self, epoch):
        for step in range(self.n_train_steps):
            # Get batch from generator
            (x_train, y_train) = next(self.train_gen)

            if step == 0:
                saved_image = x_train
                saved_target = y_train

            if isinstance(self.loss_fn, (list, tuple)) and len(self.loss_fn) > 1:
                # Train batch
                logits, loss_value, gradients = train_batch_mask_sup(
                    model=self.model, 
                    optimizer=self.optimizer, 
                    loss_fn=self.loss_fn, 
                    x_train=x_train, 
                    y_train=y_train
                    )
            else:
                # Train batch
                logits, loss_value, gradients = self.train_batch(
                    model=self.model, 
                    optimizer=self.optimizer, 
                    loss_fn=self.loss_fn, 
                    x_train=x_train, 
                    y_train=y_train
                    )

            preds = self.model(saved_image, training=False)
            # self.train_logger.log_hist(gradients, step)
            self.train_logger.log_image('saved_image', preds, step)
            # self.train_logger.log_image('mask', preds, step)
            # # self.train_logger.log_image('diff', tf.abs(tf.math.subtract(saved_target, preds)), step)   

            # Calculate Metrics
            [metric.update_state(y_train, logits) for metric in self.metrics]
            if step % 10 == 0:
                print(f'Loss at step {step}: {loss_value}')
                print(f'Seen so far: {(step + 1) * self.config.BATCH_SIZE} samples')

        # Epoch Metrics
            [self.train_logger.log_value(metric.name, metric.result(), step) for metric in self.metrics]
            self.train_logger.log_value('unnormalised MSE', tf.metrics.MeanSquaredError()((y_train*255).astype('uint8'), (logits.numpy()*255).astype('uint8')), step)
            if type(x_train) == list:
                self.train_logger.log_value('baseline', tf.metrics.MeanAbsoluteError()(x_train[0], y_train*2 - 1), step)
            else:
                self.train_logger.log_value('baseline', tf.metrics.MeanAbsoluteError()(x_train, y_train*2 - 1), step)
        [metric.reset_states() for metric in self.metrics]

    def validate_epoch(self, epoch, prev_val_loss):
        # Validation pass
        for idx in range(self.n_val_steps):
            # Get batch from generator
            (x_val, y_val) = next(self.val_gen)

            # Get validation loss
            val_logits, val_loss = self.validate_batch(
                model=self.model, 
                loss_fn=self.loss_fn, x_val=x_val, y_val=y_val)
            # epoch_val_loss.append(val_loss)

            # Log image
            if idx == 10:
                self.val_logger.log_image('Val Image', val_logits, epoch)
            [metric.update_state(y_val, val_logits) for metric in self.metrics]

        # Save model
        m_val_loss = self.metrics[0].result()
        if  m_val_loss < prev_val_loss:
            print(
                f'''Val loss improved from {prev_val_loss} to {m_val_loss}. 
                    Saving model in {self.save_dir}'''
            )
            self.model.save(os.path.join(self.save_dir, 'best_model.hdf5'))
            prev_val_loss = m_val_loss
        
        # Validation metrics       
        [self.val_logger.log_value(metric.name, metric.result(), epoch)
        for metric in self.metrics]
        [metric.reset_states() for metric in self.metrics]

        return prev_val_loss

    def train(self):
        prev_val_loss = np.inf
        for epoch in range(self.config.EPOCHS):
            self.train_epoch(epoch)
            prev_val_loss = self.validate_epoch(epoch, prev_val_loss)
        # Stop and shut generators once done training
        self.train_q.stop()
        self.val_q.stop()

        return self.model 
                

class NeuralNetworkExperiment:
    '''
    Convenience class for building training pipelines with 
    comet, tensorboard and a variety of models.
    This class requires an instance of NeuralNetworkConfig.
    :param config: a NeuralNetworkConfig object
    :param logdir: path to store training results
    '''
    def __init__(self, config, save_dir, experiment_name=None, tags=None, weights=None):
        self.config = config
        self.experiment_name = experiment_name
        self.tags = tags
        self.weights = weights
        self.job_dir, self.model_dir, self.log_dir = self._make_paths(save_dir)

    def _make_paths(self, save_dir):
        return make_train_dirs(save_dir, self.config.EXPERIMENT_CLASS)

    def _build(self):
        '''Build and return a keras Model object'''
        model ='build_a_model'
        return model 

    def _compile(self, model):
        optimizer = tf.keras.optimizers.Adam()
        model.compile(optimizer, loss='mse', learning_rate=self.config.LEARNING_RATE) 
        return model

    def _config_data(self):
        '''Prepare a training and validation generator'''
        train_gen = 'training_generator'
        val_gen = 'validation_generator'
        return train_gen, val_gen

    def _config_callbacks(self):
        tensorboard = tf.keras.callbacks.TensorBoard(
            log_dir=self.log_dir,
            update_freq='epoch'
        )
        model_ckpt_path = os.path.join(self.model_dir, 'best_model.hdf5')
        model_checkpoint = tf.keras.callbacks.ModelCheckpoint(
            filepath=model_ckpt_path,
            monitor='val_loss',
            verbose=1,
            save_best_only=True,
            save_freq='epoch'
        )
        return [tensorboard, model_checkpoint]

    def _config_comet(self, tags=None):
        if self.config.COMET_API_KEY is not None and self.config.COMET_PROJECT is not None:
            experiment = create_comet_experiment(
                self.config.COMET_API_KEY, 
                self.config.COMET_PROJECT,
                experiment_name=self.experiment_name,
                tags=self.tags
                )
            experiment.log_parameters(self.config.__dict__)
            return experiment

    def train(self):
        if self.weights is not None:
            model = tf.keras.models.load_model(self.weights, compile=False)
        else:
            model = self._build()
        model = self._compile(model)
        train_gen, val_gen = self._config_data()
        callbacks = self._config_callbacks()
        comet = self._config_comet()
        print(f'Cardinality {tf.data.experimental.cardinality(train_gen).numpy()}')
        model.fit(
            x=train_gen,
            epochs=self.config.EPOCHS,
            steps_per_epoch=tf.data.experimental.cardinality(train_gen).numpy()//self.config.EPOCHS,
            callbacks=callbacks,
            validation_data=val_gen,
            validation_steps=tf.data.experimental.cardinality(val_gen).numpy()//self.config.EPOCHS,
            max_queue_size=self.config.MP_MAX_QUEUE_SIZE,
            workers=self.config.MP_WORKERS,
            use_multiprocessing=self.config.MULTIPROCESSING,
            shuffle=True
        )


class NNExperiment:
    '''
    CUSTOM TRAINING LOOP!
    Convenience class for building training pipelines with 
    comet, tensorboard and a variety of models.
    This class requires an instance of NeuralNetworkConfig.
    :param config: a NeuralNetworkConfig object
    :param logdir: path to store training results
    '''
    def __init__(self, config, save_dir, experiment_name=None, tags=None, weights=None):
        self.config = config
        self.experiment_name = experiment_name
        self.tags = tags
        self.weights = weights
        self.job_dir, self.model_dir, self.log_dir = self._make_paths(save_dir)

    def _make_paths(self, save_dir):
        return make_train_dirs(save_dir, self.config.EXPERIMENT_CLASS)

    def _build(self):
        '''Build and return a keras Model object'''
        model ='build_a_model'
        return model 

    def _compile(self):
        optimizer = tf.keras.optimizers.Adam(
            learning_rate=self.config.LEARNING_RATE
            )
        loss = tf.keras.losses.MAE
        return optimizer, loss

    def _config_data(self):
        '''Prepare a training and validation generator'''
        train_gen = 'training_generator'
        val_gen = 'validation_generator'
        return train_gen, val_gen

    def _config_comet(self, tags=None):
        if self.config.COMET_API_KEY is not None and self.config.COMET_PROJECT is not None:
            experiment = create_comet_experiment(
                self.config.COMET_API_KEY, 
                self.config.COMET_PROJECT,
                experiment_name=self.experiment_name,
                tags=self.tags
                )
            experiment.log_parameters(self.config.__dict__)
            return experiment

    def train(self):
        if self.weights is not None:
            model = tf.keras.models.load_model(self.weights, compile=False)
        else:
            model = self._build()
        optimizer, loss = self._compile()
        train_gen, val_gen = self._config_data()
        comet = self._config_comet()
        trainer = TrainingLoop(
            model=model, 
            loss_fn=loss, 
            optimizer=optimizer,
            train_gen=train_gen, 
            val_gen=val_gen,
            config=self.config, 
            metrics=[tf.keras.metrics.MeanAbsoluteError(name='mae')],
            logdir=self.log_dir,
            savedir=self.model_dir
            )
        trainer.train()


def make_job_path(save_dir, experiment_class):
    dt = datetime.datetime.now().strftime('%Y-%m-%d_%H_%M_%S')
    return os.path.join(save_dir, f'{experiment_class}_{dt}')


def make_train_dirs(save_dir, experiment_name):
    job_dir = make_job_path(save_dir, experiment_name)
    model_dir = os.path.join(job_dir, 'models')
    log_dir = os.path.join(job_dir, 'logs')
    dirs_to_make = (save_dir, job_dir, model_dir, log_dir)
    [os.mkdir(d) for d in dirs_to_make if not os.path.exists(d)]
    logging.info(f"Created directories: {', '.join(dirs_to_make)}")
    return job_dir, model_dir, log_dir


def create_comet_experiment(api_key, project_name, experiment_name=None, tags=None):
    experiment = Experiment(
        api_key=api_key,
        project_name=project_name,
        workspace="dolhasz"
    )
    if isinstance(tags, list):
        experiment.add_tags(tags)
    else:
        experiment.add_tag(tags)
    if experiment_name is not None:
        experiment.set_name(experiment_name)
    return experiment


if __name__ == "__main__":
    config = NeuralNetworkConfig()
    print(config.EXPERIMENT_CLASS)
    net = NeuralNetworkExperiment(config, './training_results')