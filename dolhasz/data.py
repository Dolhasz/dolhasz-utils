import os
import cv2
import socket
import numpy as np
import dolhasz as ad
import tensorflow as tf
from tensorflow.keras.utils import Sequence, to_categorical
from pycocotools.coco import COCO


class COCOGenerator(Sequence):
    def __init__(self, coco_path, batch_size=1, 
    image_shape=None, n_samples=1000, train_val='train2017', preprocess=False):
        self.coco_path = coco_path
        self.batch_size = batch_size
        self.image_shape = image_shape
        self.train_val = train_val
        self.preprocess = preprocess
        self.coco, self.categories = init_coco(coco_path, train_val)
        if n_samples is not None:
            self.image_ids = self.coco.getImgIds()[:n_samples]
        else:
            self.image_ids = self.coco.getImgIds()
        self.n_images = len(self.image_ids)

    def load_image(self, image_id_list):
        if not isinstance(image_id_list, list):
            image_id_list = [image_id_list]
        coco_img = self.coco.loadImgs(image_id_list)[0]
        fname = f'{self.coco_path}/images/{self.train_val}/{coco_img["file_name"]}'
        np_img = ad.image.read(fname, dtype='uint')
        return np_img, coco_img

    def load_mask(self, coco_img, cat_filter=None):
        ann_ids = self.coco.getAnnIds(imgIds=coco_img['id'], iscrowd=None)
        anns = self.coco.loadAnns(ann_ids)
        if len(anns) < 1:
            print('No mask')
            return np.zeros(self.image_shape), None
        if cat_filter is not None:
            anns = [a for a in anns if int(a['category_id']) == int(cat_filter)]
        annotation = np.random.choice(anns)
        mask = self.coco.annToMask(annotation)
        return mask, annotation

    def sample_category(self, cat_id):
        img_ids = self.coco.getImgIds(catIds=cat_id)
        return np.random.choice(img_ids)

    def sample_same_category(self, coco_ann):
        img_id = self.sample_category(coco_ann['category_id'])
        image, coco_img = self.load_image(img_id)
        mask, coco_ann = self.load_mask(
            coco_img=coco_img, 
            cat_filter=coco_ann['category_id']
            )
        return image, coco_img, mask, coco_ann

    def load_images_and_masks(self, indices):
        images = []
        masks = []
        for index in indices:
            image, coco_img = self.load_image(index)
            mask, coco_ann = self.load_mask(coco_img)
            if self.image_shape is not None:
                image = cv2.resize(image, self.image_shape[:2])
                mask = cv2.resize(image, self.image_shape[:2])
            images.append(image)
            masks.append(mask)
        return images, masks

    def load_transfer_colour(self, indices):
        src_imgs = []
        res_imgs = []
        transfer_masks = []
        binary_masks = []
        for index in indices:
            source, coco_img = self.load_image(index)
            src_mask, coco_ann = self.load_mask(coco_img)

            if np.sum(src_mask) > 0.0:
                target, _, tgt_mask, _ = self.sample_same_category(coco_ann)
                result, source, target = ad.image.color_transfer(
                    source=source, 
                    target=target, 
                    source_mask=src_mask, 
                    target_mask=tgt_mask
                )  
                transfer_mask = np.log2(np.divide(
                    np.clip(result, 1, 255), 
                    np.clip(source, 1, 255)
                    ))
                transfer_mask = transfer_mask * np.dstack([src_mask]*3)
                transfer_mask = transfer_mask / 8.0
                src_imgs.append(cv2.resize(source, self.image_shape[:2])/255.0)
                res_imgs.append(cv2.resize(result, self.image_shape[:2])/255.0)
                transfer_masks.append(cv2.resize(transfer_mask, self.image_shape[:2]))
                binary_masks.append(to_categorical(cv2.resize(src_mask, self.image_shape[:2]), num_classes=2))
            else:
                src_imgs.append(cv2.resize(source, self.image_shape[:2])/255.0)
                res_imgs.append(cv2.resize(source, self.image_shape[:2])/255.0)
                transfer_masks.append(np.zeros(self.image_shape))
                binary_masks.append(to_categorical(np.zeros(self.image_shape[:2]), num_classes=2))

        return res_imgs, src_imgs, transfer_masks, binary_masks

    def __len__(self):
        return int(np.ceil(self.n_images / self.batch_size))

    def __getitem__(self, idx):
        indices = self.image_ids[
            idx * self.batch_size : (idx + 1) * self.batch_size
        ]
        X, Y1, Y2, Y3 = self.load_transfer_colour(indices)
        if self.preprocess:
            X = ad.image.preprocess_input(X)
        return np.array(X), [np.array(Y1), np.array(Y2), np.array(Y3)]


def init_coco(coco_dir, train_val):
    annotation_file = f'{coco_dir}/annotations/instances_{train_val}.json'
    coco = COCO(annotation_file)
    categories = [c['name'] for c in coco.loadCats(coco.getCatIds())]
    return coco, categories


HARMONIZATION_DATASETS = (
    'HCOCO',
    'HAdobe5k',
    'Hday2night',
    'HFlickr',
)

MACHINE_NAME = socket.gethostname()

def GET_HARMONIZATION_PATH():
    if MACHINE_NAME == 'Dolhasz':
        DATA_PATH = 'E:/image_harmonization'
    elif MACHINE_NAME == 'DESKTOP-8K2EUHF':
        DATA_PATH = 'H:/image_harmonization'
    elif MACHINE_NAME == 'dolhasz-laptop':
        DATA_PATH = 'D:/image_harmonization'
    elif MACHINE_NAME == 'neuron':
        DATA_PATH = '/home/dolhasz/Image_Harmonization_Dataset'
    elif MACHINE_NAME == 'neuromancer':
        DATA_PATH = '/home/dolhasz/Image_Harmonization_Dataset'
    elif MACHINE_NAME == 'serrano':
        DATA_PATH = '/home/dolhasz/image_harmonization/'
    return DATA_PATH


def multithread_generator(gen, workers, queue_size):
    q = tf.keras.utils.OrderedEnqueuer(gen)
    q.start(
        workers = workers,
        max_queue_size = queue_size
    )
    gen = q.get()
    return q, gen



class HarmonizationGenerator(Sequence):
    def __init__(self, path, batch_size=1, 
        image_shape=None, n_samples=None, dataset='HCOCO', 
        train=True, preprocess=True, mask_in=False):

        self.path = path
        self.batch_size = batch_size
        self.image_shape = image_shape
        self.n_samples = n_samples
        self.dataset = dataset
        self.train = train

        if 'all' in dataset:
            self.images = self.load_all_datasets()
        else:
            self.images = self.load_image_names(dataset)
        if self.n_samples is not None:
            self.images = self.images[:n_samples]
        if self.train:
            np.random.shuffle(self.images)
        self.n_images = len(self.images)
        self.preprocess = preprocess
        self.mask_in = mask_in
        
    def load_image_names(self, dataset):
        full_path = os.path.join(
            self.path,
            dataset,
            f'{dataset}_{"train" if self.train else "test"}.txt'
            )
        with open(full_path, 'r') as image_list:
            image_names = []
            for image_name in image_list.readlines():
                image_names.append([dataset, image_name.strip('\n')])
            return image_names

    def load_all_datasets(self):
        all_images = []
        for dataset in HARMONIZATION_DATASETS:
            image_paths = self.load_image_names(dataset)
            print(f'Number of images in {dataset} dataset: {len(image_paths)}')
            all_images.extend(image_paths)
        return all_images

    def load_image(self, image_name, imtype, dataset):
        path = os.path.join(self.path, dataset, imtype, image_name)
        path = os.path.normpath(path)
        gray = True if imtype is 'masks' else False
        return ad.image.read(path, dtype='float', resize=self.image_shape[:2], gray=gray)

    def load_real(self, image_name, dataset):
        name, _, comp = image_name.split('_')
        _, ext = comp.split('.')
        return self.load_image(f'{name}.{ext}', 'real_images', dataset)

    def load_composite(self, image_name, dataset):
        return self.load_image(image_name, 'composite_images', dataset)

    def load_mask(self, image_name, dataset):
        name, mask, comp = image_name.split('_')
        _, ext = comp.split('.')
        return self.load_image(f'{name}_{mask}.{"png"}', 'masks', dataset)

    def load_all(self, image_name, dataset):
        return (
            self.load_composite(image_name, dataset),
            self.load_real(image_name, dataset),
            self.load_mask(image_name, dataset)
        )

    def load_batch(self, image_names):   
        comps, images, masks = [], [], []
        for dataset, name in image_names:
            comp, image, mask = self.load_all(name, dataset)
            comps.append(comp)
            images.append(image)
            masks.append(mask)
        return comps, images, masks

    def __len__(self):
        return int(np.ceil(self.n_images / self.batch_size))

    def __getitem__(self, idx):
        image_names = self.images[
            idx * self.batch_size : (idx + 1) * self.batch_size
        ]
        comps, images, masks = self.load_batch(image_names)
        if self.preprocess:
            comps = ad.image.preprocess_input(comps)
        if self.mask_in == 'nomask':
            return np.array(comps).astype('float32'), np.array(images).astype('float32')
        elif self.mask_in:
            masks = np.array(masks).reshape(-1, *self.image_shape[:2], 1)
            return [np.array(comps).astype('float32'), masks.astype('float32')], np.array(images).astype('float32')
        else:
            return np.array(comps).astype('float32'), (np.array(images).astype('float32'), np.array(masks).astype('float32'))

    def on_epoch_end(self):
        if self.train:
            np.random.shuffle(self.images)
            print(self.images[0])


### NEW GENERATORS !

def load_image_names(path, dataset, train=True):
    full_path = os.path.join(
        path,
        dataset,
        f'{dataset}_{"train" if train else "test"}.txt'
        )
    with open(full_path, 'r') as image_list:
        X = []
        Y = []
        M = []
        for image_name in image_list.readlines():
            X.append(make_path(image_name.strip('\n'), dataset, path, 'comp'))
            Y.append(make_path(image_name.strip('\n'), dataset, path, 'real'))
            M.append(make_path(image_name.strip('\n'), dataset, path, 'mask'))
        return X, Y, M


def make_path(image_name, dataset, dataset_path, ptype='real'):
    if ptype == 'real':
        name, _, comp = image_name.split('_')
        _, ext = comp.split('.')
        return os.path.join(dataset_path, dataset, 'real_images', name + '.' + ext)
    elif ptype == 'comp':
        return os.path.join(dataset_path, dataset, 'composite_images', image_name)
    elif ptype == 'mask':
        name, mask, comp = image_name.split('_')
        _, ext = comp.split('.')
        return os.path.join(dataset_path, dataset, 'masks', f'{name}_{mask}.{"png"}')

def load_both_images(path_c, path_r, output_paths=False):
    x = tf.io.read_file(path_c)
    x = tf.io.decode_jpeg(x, channels=3)
    x = tf.image.resize(x, (256, 256)) 
    x = tf.image.convert_image_dtype(x, dtype='float32') / 255. * 2 - 1

    y = tf.io.read_file(path_r)
    y = tf.io.decode_jpeg(y, channels=3)
    y = tf.image.resize(y, (256, 256))
    y = tf.image.convert_image_dtype(y, dtype='float32') / 255.

    if output_paths:
        return x, y, path_c
    else:
        return x, y


def load_images_mask(path_c, path_m, path_r, output_paths=False):
    x = tf.io.read_file(path_c)
    x = tf.io.decode_jpeg(x, channels=3)
    x = tf.image.resize(x, (256, 256)) 
    x = tf.image.convert_image_dtype(x, dtype='float32') / 255. * 2 - 1

    m = tf.io.read_file(path_m)
    m = tf.io.decode_jpeg(m, channels=1)
    m = tf.image.resize(m, (256, 256))
    m = tf.image.convert_image_dtype(m, dtype='float32') / 255.

    y = tf.io.read_file(path_r)
    y = tf.io.decode_jpeg(y, channels=3)
    y = tf.image.resize(y, (256, 256))
    y = tf.image.convert_image_dtype(y, dtype='float32') / 255.

    if output_paths:
        return x, m, y, path_c
    else:
        return x, m, y


def concat_all_datasets(dataset_path, training):
    Xs, Ys, Ms = [], [], []
    for dataset in HARMONIZATION_DATASETS:
        x, y, m = load_image_names(dataset_path, dataset, train=training)
        Xs.extend(x)
        Ys.extend(y)
        Ms.extend(m)

    return Xs, Ys, Ms

def TfHarmonizerGenerator(dataset, batch_size, training=True, epochs=10, output_paths=False, mask_in=False):
    dataset_path = GET_HARMONIZATION_PATH()
    if 'all' in dataset:
        Xs, Ys, Ms = concat_all_datasets(dataset_path, training)
    else:
        Xs, Ys, Ms = load_image_names(dataset_path, dataset, train=training)

    if not mask_in:
        ds = tf.data.Dataset.from_tensor_slices((Xs, Ys))
        if training:
            ds = ds.shuffle(len(Xs))
        ds = ds.map(lambda x, y: load_both_images(x, y, output_paths=output_paths), num_parallel_calls=tf.data.experimental.AUTOTUNE)
        ds = ds.batch(batch_size, drop_remainder=True if training else False)
        ds = ds.repeat(epochs)
        ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
        return ds
    else:
        ds = tf.data.Dataset.from_tensor_slices((Xs, Ms, Ys))
        if training:
            ds = ds.shuffle(len(Xs))
        ds = ds.map(lambda x, m, y: load_images_mask(x, m, y, output_paths=output_paths), num_parallel_calls=tf.data.experimental.AUTOTUNE)
        ds = ds.batch(batch_size, drop_remainder=True if training else False)
        ds = ds.repeat(epochs)
        ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
        return ds

if __name__ == "__main__":

    image_shape = (256,256,3)

    # gen = HarmonizationGenerator('D:/image_harmonization', batch_size=32, 
    #     image_shape=image_shape, n_samples=None, dataset='HCOCO', 
    #     train=True, preprocess=True, mask_in='nomask')
    gen = TfHarmonizerGenerator('all', 12*3)
    for x, y, p in gen.take(1):
        print(p)
    quit()
    with tf.distribute.MirroredStrategy().scope():
        model = tf.keras.models.Sequential([
            tf.keras.layers.Conv2D(3, (3,3), padding='same', input_shape=(image_shape))
        ])

        model.compile('Adam', 'mse')
        print(tf.data.experimental.cardinality(gen).numpy()//10)
        model.fit(gen, steps_per_epoch=tf.data.experimental.cardinality(gen).numpy()//10, verbose=1)
