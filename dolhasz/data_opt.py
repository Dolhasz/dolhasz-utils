'''
Optimised data loaders for TF2
'''
import os
import tensorflow as tf
import numpy as np
from dolhasz.data import GET_HARMONIZATION_PATH, HARMONIZATION_DATASETS


def load_image_names(path, dataset, train=True):
    full_path = os.path.join(
        path,
        dataset,
        f'{dataset}_{"train" if train else "test"}.txt'
    )
    with open(full_path, 'r') as image_list:
        X, Y, M = [], [], []
        for image_name in image_list.readlines():
            X.append(make_path(image_name.strip('\n'), dataset, path, 'comp'))
            Y.append(make_path(image_name.strip('\n'), dataset, path, 'real'))
            M.append(make_path(image_name.strip('\n'), dataset, path, 'mask'))
        return X, Y, M


def make_path(image_name, dataset, dataset_path, ptype='real'):
    if ptype == 'real':
        name, _, comp = image_name.split('_')
        _, ext = comp.split('.')
        return os.path.join(dataset_path, dataset, 'real_images', name + '.' + ext)
    elif ptype == 'mask':
        name, mask, comp = image_name.split('_')
        _, ext = comp.split('.')
        return os.path.join(dataset_path, dataset, 'masks', f'{name}_{mask}.{"png"}')
    elif ptype == 'comp':
        return os.path.join(dataset_path, dataset, 'composite_images', image_name)


def concat_all_datasets(dataset_path, training):
    Xs, Ys, Ms = [], [], []
    for dataset in HARMONIZATION_DATASETS:
        x, y, m = load_image_names(dataset_path, dataset, train=training)
        Xs.extend(x)
        Ys.extend(y)
        Ms.extend(m)
    return Xs, Ys, Ms


def load_img(path, image_shape=(256,256,3), preprocess=False):
    x = tf.io.read_file(path)
    x = tf.io.decode_jpeg(x, channels=image_shape[-1])
    x = tf.image.resize(x, image_shape[:2]) 
    x = tf.image.convert_image_dtype(x, dtype='float32') / 255.
    if preprocess:
        x = x * 2 - 1
    return x


def load_no_mask(comp_path, img_path, augment=False):
    x = load_img(comp_path, preprocess=True)
    y = load_img(img_path, preprocess=False)
    if augment:
        seed = np.random.randint(9999999)
        x = tf.image.random_flip_left_right(x, seed=seed)
        y = tf.image.random_flip_left_right(y, seed=seed)

        seed = np.random.randint(9999999)
        x = tf.image.random_flip_up_down(x, seed=seed)
        y = tf.image.random_flip_up_down(y, seed=seed)
    return x, y


def load_mask_in(comp_path, mask_path, img_path):
    return (
        (
            load_img(comp_path, preprocess=False),
            load_img(mask_path, image_shape=(256,256,1), preprocess=False)
        ),
        load_img(img_path, preprocess=False)
    )
    

def load_mask_out(comp_path, mask_path, img_path):
    return (
            load_img(comp_path, preprocess=False),
        (
            load_img(img_path, preprocess=False),
            load_img(mask_path, image_shape=(256,256,1), preprocess=False)
        )
    )


class iHarmonyGenerator:
    def __init__(self, dataset='all', batch_size=32, epochs=1, training=True, augment=False):
        self.dataset_path = GET_HARMONIZATION_PATH()
        self.dataset = dataset
        self.batch_size = batch_size
        self.epochs = epochs
        self.training = training
        self.augment = augment

        if 'all' in dataset:
            self.Xs, self.Ys, self.Ms = concat_all_datasets(self.dataset_path, training)
        else:
            self.Xs, self.Ys, self.Ms = load_image_names(self.dataset_path, dataset, train=training)


    def masks_in(self):
        l = lambda x, m, y: load_mask_in(x, m, y)
        ds = self.make((self.Xs, self.Ms, self.Ys), l)
        return ds

    def masks_out(self):
        l = lambda x, m, y: load_mask_out(x, m, y)
        ds = self.make((self.Xs, self.Ms, self.Ys), l)
        return ds

    def no_masks(self):
        l = lambda x, m, y: load_no_mask(x, y, augment=self.augment)
        ds = self.make((self.Xs, self.Ms, self.Ys), l)
        return ds


    def make(self, slices, lmb):
        ds = tf.data.Dataset.from_tensor_slices(slices)
        if self.training:
            ds = ds.shuffle(len(slices[0]))
        ds = ds.map(lmb, num_parallel_calls=tf.data.experimental.AUTOTUNE)
        ds = ds.batch(self.batch_size, drop_remainder=True if self.training else False)
        ds = ds.repeat(self.epochs)
        ds = ds.prefetch(tf.data.experimental.AUTOTUNE)
        return ds



if __name__ == "__main__":
    gen = iHarmonyGenerator().masks_in()

    i1 = tf.keras.layers.Input(shape=(256,256,3))
    i2 = tf.keras.layers.Input(shape=(256,256,1))

    t = tf.keras.layers.Concatenate(axis=-1)([i1, i2])
    x = tf.keras.layers.Conv2D(3, 3, padding='same')(t)
    # x2 = tf.keras.layers.Conv2D(1, 3, padding='same')(i1)
    model = tf.keras.Model([i1, i2], x)
    model.compile('adam', 'mse')
    model.fit(gen)
