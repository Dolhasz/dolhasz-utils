#!/usr/bin/env python
import setuptools
from distutils.core import setup

setup(name='Dolhasz: Deep Learning Utils',
      version='1.0',
      description='Various utils used for image-based deep learning projects',
      author='Alan Dolhasz',
      author_email='alan@dolhasz.com',
      url='https://gitlab.com/Dolhasz/dolhasz-utils',
      packages=['dolhasz'],
      python_requires='>=3.6'
     )